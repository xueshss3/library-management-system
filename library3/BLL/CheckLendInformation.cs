﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL;
using Model;
namespace BLL
{

    public class CheckLendInformation
    {
       
        GetInformationfromSqlserver getInformationfromSqlserver;

        

        public  DataTable find_all_lend_information()
        {
            getInformationfromSqlserver = new GetInformationfromSqlserver();
            string cmdstr = "select *from lend";
            getInformationfromSqlserver.CmdString = cmdstr;
            DataTable dt = new DataTable();
            dt = getInformationfromSqlserver.selectInfo();
            return dt;
        }
        public int lend_book(TransferBook transferBook)
        {
            int checkflag = 0;
            getInformationfromSqlserver = new GetInformationfromSqlserver();
            DataTable dt = new DataTable();
            string cmdstr1 = "select * from book1 where bid=" + transferBook.Bid;
            getInformationfromSqlserver.CmdString = cmdstr1;
            dt = getInformationfromSqlserver.selectInfo();
            if(dt.Rows.Count==0)
            {
                return checkflag;
            }
            else if (Convert.ToInt32(dt.Rows[0]["bnumber"]) > 0)
            {
                string cmdstr = "update book1 set bnumber=bnumber-1 where bid=" + transferBook.Bid;
                getInformationfromSqlserver.CmdString = cmdstr;
                checkflag = getInformationfromSqlserver.executeInfo();
                return checkflag;
            }
            else return checkflag;
            

        }

        public int insert_lend_information(TransferAdmin transferAdmin,TransferBook transferBook)
        {
            DataTable dt = new DataTable();
            
            
            string daystr = DateTime.Now.ToString("yyyy-MM-dd");
            getInformationfromSqlserver = new GetInformationfromSqlserver();
            string cmdstr = "select * from book1 where bid=" + transferBook.Bid;
            getInformationfromSqlserver.CmdString = cmdstr;
            dt = getInformationfromSqlserver.selectInfo();
            string cmdstr1 = "insert into lend values('"+transferAdmin.Name+"',"+transferBook.Bid+",'"+dt.Rows[0]["bname"].ToString()+"','"+daystr+"')";
            getInformationfromSqlserver.CmdString = cmdstr1;
            int checkflag = getInformationfromSqlserver.executeInfo();
            return checkflag;
        }

        public int return_book(TransferBook transferBook)
        {
            int checkflag = 0;
            getInformationfromSqlserver = new GetInformationfromSqlserver();
            string cmdstr = "update book1 set bnumber=bnumber+1 where bid=" + transferBook.Bid;
            getInformationfromSqlserver.CmdString = cmdstr;
            checkflag = getInformationfromSqlserver.executeInfo();
            return checkflag;

        }
    }

    
}

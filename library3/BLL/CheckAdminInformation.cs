﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Model;
using DAL;


namespace BLL
{
    public class CheckAdminInformation
    {
        private GetInformationfromSqlserver getInformationfromSqlserver = null;
        private TransferAdmin user = null;//定义User 对象，接收Form1传过来登陆对象信息，在此用来校验
        private TransferAdmin userback = null;//定义一个新的User对象，如果搜索到符合的记录后，将此对象初始化并返回

        public CheckAdminInformation(TransferAdmin user)
        {
            getInformationfromSqlserver = new GetInformationfromSqlserver();
            this.user = user;
        }

        
        //注册处理方法
        public int sign_up()
        {
            int res;
            string logstr = "insert into admin1(name,pwd,role) values('" +user.Name + "','" +user.Pwd + "','" + user.Role + "')";
            getInformationfromSqlserver.CmdString = logstr; //对Command 对象的属性赋值
            res = getInformationfromSqlserver.executeInfo();
            return res;

        }

        //登录
        public TransferAdmin sign_in()
        {
            DataTable dt = new DataTable();
            string cmdstr = "select * from admin1 where name='"+ user.Name +"'and pwd='"+user.Pwd+"'";
            getInformationfromSqlserver.CmdString = cmdstr;
            dt = getInformationfromSqlserver.selectInfo();
            if (dt.Rows.Count > 0)
            {
                userback = new TransferAdmin();//用户存在，对象初始化
                userback.Name = dt.Rows[0]["name"].ToString();
                userback.Role = dt.Rows[0]["role"].ToString();
                
            }
            return userback;
        }

    

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using Model;

namespace BLL
{
    public class CheckBookInformation
    {
        GetInformationfromSqlserver getInformationfromSqlserver;
        
        public CheckBookInformation()
        {
            getInformationfromSqlserver = new GetInformationfromSqlserver();
        }

        public DataTable find_all()
        {

            DataTable dt = new DataTable();
            string cmdstr = "select bid as 书号 ,bname as 书名,bauthor as 作者,bpublish as 出版社 ,bnumber as 数量 from book1";
            getInformationfromSqlserver.CmdString = cmdstr;
            dt = getInformationfromSqlserver.selectInfo();
            return dt;

        }


        public DataTable lend_book(TransferBook transferBook)
        {
            DataTable dt = new DataTable();
            return dt;
        }


        public int  add_book(TransferBook transferBook)
        {
            int checkflag = 0;
            string cmdstr = "insert into book1 values(" + transferBook.Bid + ",'" + transferBook.Bname + "','" + transferBook.Bauthor + "','" + transferBook.Bpublish + "'," + transferBook.Bnumber + ")";
            getInformationfromSqlserver.CmdString = cmdstr;
            checkflag=getInformationfromSqlserver.executeInfo();
            return checkflag;
        }

        public int delete_book(TransferBook transferBook)
        {
            int checkflag = 0;
            string cmdstr = "delete from book1 where bid=" + transferBook.Bid + "and bname='" + transferBook.Bname + "'";
            getInformationfromSqlserver.CmdString = cmdstr;
            checkflag = getInformationfromSqlserver.executeInfo();
            return checkflag;
        }

        public TransferBook find_book(TransferBook transferBook)
        {
            TransferBook transferBook1 = new TransferBook();
            string cmdstr = "select* from book1 where bid='" + transferBook.Bid +"'";
            getInformationfromSqlserver.CmdString = cmdstr;
            DataTable dt= getInformationfromSqlserver.selectInfo();
            if(dt.Rows.Count>0)
            {
                transferBook1.Bname = dt.Rows[0]["bname"].ToString();
                transferBook1.Bauthor = dt.Rows[0]["bauthor"].ToString();
                transferBook1.Bpublish = dt.Rows[0]["bpublish"].ToString();
                transferBook1.Bnumber =Convert.ToInt32( dt.Rows[0]["bnumber"]);
            }
            else
            {
                transferBook1 = null;
            }
            return transferBook1;

        }
        public int change_book(TransferBook transferBook)
        {
            int checkflag = 0;
            string cmdstr = "update book1 set bname='"+transferBook.Bname+"',bauthor='"+transferBook.Bauthor+"',bpublish='"+transferBook.Bpublish+"',bnumber="+transferBook.Bnumber+"where bid="+transferBook.Bid;
            getInformationfromSqlserver.CmdString = cmdstr;
            checkflag = getInformationfromSqlserver.executeInfo();
            return checkflag;
        }
    }
}

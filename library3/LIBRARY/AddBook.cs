﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;
using BLL;


namespace LIBRARY
{
    public partial class AddBook : Form
    {
        TransferBook transferBook;
        public AddBook()
        {
            InitializeComponent();
        }

        private void AddBook_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int checkflag = 0;
            transferBook = new TransferBook();
            transferBook.Bid = Convert.ToInt32(textBox1.Text);
            transferBook.Bname = textBox2.Text;
            transferBook.Bauthor = textBox3.Text;
            transferBook.Bpublish = textBox4.Text;
            transferBook.Bnumber = Convert.ToInt32(textBox5.Text);
            CheckBookInformation checkBookInformation = new CheckBookInformation();
            checkflag = checkBookInformation.add_book(transferBook);
            if(checkflag>0)
            {
                MessageBox.Show("添加成功");
                


            }
            else
            {
                MessageBox.Show("添加失败");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            AdministrationMenu administrationMenu = new AdministrationMenu();
            administrationMenu.Show();
            this.Close();
        }
    }
}

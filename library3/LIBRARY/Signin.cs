﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using Model;

namespace LIBRARY
{
    public partial class Signin : Form
    {
        TransferAdmin user;
        public Signin()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            user = new TransferAdmin();
            TransferAdmin userback = new TransferAdmin();
            if(textBox1.Text==""||textBox2.Text=="" )
            {
                MessageBox.Show("输入信息不能为空","提示",MessageBoxButtons.OKCancel,MessageBoxIcon.Warning);
            }
            else
            {
                user.Name = textBox1.Text;
                user.Pwd = textBox2.Text;
                user.Role = comboBox1.SelectedItem.ToString();
                CheckAdminInformation blog = new CheckAdminInformation(user);
                userback = blog.sign_in();
                string pattern = user.Role + @" +";
                Regex regex = new Regex(pattern);
                MatchCollection matchs1 = null;
                if (userback != null)
                {
                    matchs1 = regex.Matches(userback.Role);
                    if (matchs1.Count > 0)
                    {
                        if (user.Role == "管理员")
                        {
                            AdministrationMenu administrationMenu = new AdministrationMenu();
                            administrationMenu.Show();
                            this.Hide();

                        }
                        else
                        {
                            StudentAndTeacherMenu studentAndTeacherMenu = new StudentAndTeacherMenu(user);
                            studentAndTeacherMenu.Show();
                            this.Hide();
                        }
                    }
                    else
                    {
                        MessageBox.Show("身份不正确");
                    }

                }
                else
                {
                    MessageBox.Show("用户名或密码错误");
                }

            }



        }


        //注册
        private void button1_Click(object sender, EventArgs e)
        {
            Signup signup = new Signup();
            this.Hide();
            signup.Show();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

﻿using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;

namespace LIBRARY
{
    public partial class StudentAndTeacherMenu : Form
    {
        CheckBookInformation checkBookInformation;
        CheckLendInformation checkLendInformation;
        TransferBook transferBook;
        TransferAdmin transferAdmin;
        public StudentAndTeacherMenu()
        {
            InitializeComponent();
        }

        public StudentAndTeacherMenu(TransferAdmin transferAdmin)
        {
            InitializeComponent();
            this.transferAdmin = transferAdmin;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            checkBookInformation = new CheckBookInformation();
            dataGridView1.DataSource = checkBookInformation.find_all();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Signin signin = new Signin();
            this.Close();
            signin.Close();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Signin signin = new Signin();
            this.Close();
            signin.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int checkflag =0;
            int checkflag1 = 0;
            transferBook = new TransferBook();
            transferBook.Bid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
            checkLendInformation = new CheckLendInformation();
            checkflag= checkLendInformation.lend_book(transferBook);
            checkflag1=checkLendInformation.insert_lend_information(transferAdmin, transferBook);
            if (checkflag>0&&checkflag1>0)
            {
                
                MessageBox.Show("借书成功");
                checkBookInformation = new CheckBookInformation();
                dataGridView1.DataSource = checkBookInformation.find_all();

            }
            else
            {
                MessageBox.Show("借书失败"); 
            }
           



        }

        private void StudentAndTeacherMenu_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            int checkflag = 0;
            transferBook = new TransferBook();
            transferBook.Bid = Convert.ToInt32(textBox2.Text);
            checkLendInformation = new CheckLendInformation();
            checkflag = checkLendInformation.return_book(transferBook);
            if (checkflag > 0)
            {
                MessageBox.Show("还书成功");
                dataGridView1.DataSource = checkBookInformation.find_all();
            }
            else
            {
                MessageBox.Show("还书失败");
            }
        }
    }
}

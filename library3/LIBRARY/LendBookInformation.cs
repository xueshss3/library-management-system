﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace LIBRARY
{
    public partial class LendBookInformation : Form
    {
        CheckLendInformation checkLendInformation;
        public LendBookInformation()
        {
            InitializeComponent();
            checkLendInformation = new CheckLendInformation();
            dataGridView1.DataSource = checkLendInformation.find_all_lend_information();
        }

        private void LendBookInformation_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            AdministrationMenu administrationMenu = new AdministrationMenu();
            administrationMenu.Show();
            this.Close();

        }
    }
}

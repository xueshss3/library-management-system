﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using Model;

namespace LIBRARY
{
    public partial class AdministrationMenu : Form
    {
        CheckBookInformation checkBookInformation;
        CheckLendInformation checkLendInformation;
        public AdministrationMenu()
        {
            InitializeComponent();
            checkBookInformation = new CheckBookInformation();
            dataGridView1.DataSource = checkBookInformation.find_all();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            checkBookInformation = new CheckBookInformation();
            dataGridView1.DataSource = checkBookInformation.find_all();
        }


        //退出系统
        private void button6_Click(object sender, EventArgs e)
        {
            Signin signin = new Signin();
            this.Close();
            signin.Close();
        }


        //返回目录
        private void button5_Click(object sender, EventArgs e)
        {
            
            Signin signin = new Signin();
            this.Close();
            signin.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            LendBookInformation lendBookInformation = new LendBookInformation();
            lendBookInformation.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AddBook addBook = new AddBook();
            addBook.Show();
            this.Hide();
        }

        //修改书籍
        private void button2_Click(object sender, EventArgs e)
        {
            TransferBook transferBook = new TransferBook();
            transferBook.Bid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
            transferBook.Bname = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            transferBook.Bauthor = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            transferBook.Bpublish = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            transferBook.Bnumber= Convert.ToInt32(dataGridView1.CurrentRow.Cells[4].Value);
            ChangeBook changeBook = new ChangeBook(transferBook );
            changeBook.Show();
            this.Hide();

                
        }

        private void button4_Click(object sender, EventArgs e)
        {
            TransferBook transferBook = new TransferBook();
            
            dataGridView1.DataSource = checkBookInformation.find_all();
            transferBook.Bid=Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
            transferBook.Bname = dataGridView1.CurrentRow.Cells[1].Value.ToString();
           
            int checkflag = 0;
            checkLendInformation = new CheckLendInformation();
            checkflag = checkBookInformation.delete_book(transferBook);
            if (checkflag > 0)
            {
                MessageBox.Show("删除成功");
                dataGridView1.DataSource = checkBookInformation.find_all();
            }
            else
            {
                MessageBox.Show("添加失败");
            }
        }
    }
}

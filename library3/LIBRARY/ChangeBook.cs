﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using Model;

namespace LIBRARY
{
    public partial class ChangeBook : Form
    {
        public ChangeBook(TransferBook transferBook)
        {
            InitializeComponent();
            textBox1.Text = transferBook.Bid.ToString();
            textBox2.Text = transferBook.Bname;
            textBox3.Text = transferBook.Bauthor;
            textBox4.Text = transferBook.Bpublish;
            textBox5.Text = transferBook.Bnumber.ToString();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            TransferBook transferBook=new TransferBook();
            TransferBook transferBook1;
            transferBook.Bid =Convert.ToInt32(textBox1.Text);
            CheckBookInformation checkAdminInformation = new CheckBookInformation();
            transferBook1=checkAdminInformation.find_book(transferBook);
            if(transferBook1!=null)
            {
                textBox2.Text=transferBook1.Bname;
                textBox3.Text = transferBook1.Bauthor;
                textBox4.Text = transferBook1.Bpublish;
                textBox5.Text = transferBook1.Bnumber.ToString();
            }
            else
            {
                MessageBox.Show("查询失败");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            int checkflag=0;
            TransferBook transferBook = new TransferBook();
             transferBook.Bid = Convert.ToInt32(textBox1.Text);
             transferBook.Bname=textBox2.Text;
             transferBook.Bauthor=textBox3.Text;
             transferBook.Bpublish=textBox4.Text;
             transferBook.Bnumber=Convert.ToInt32(textBox5.Text);
             CheckBookInformation checkAdminInformation = new CheckBookInformation();
             checkflag=checkAdminInformation.change_book(transferBook);
            if(checkflag>0)
            {
                MessageBox.Show("修改成功");
              
            }
            else
            {
                MessageBox.Show("修改失败");
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            AdministrationMenu administrationMenu = new AdministrationMenu();
            administrationMenu.Show();
            this.Close();
        }
    }
}

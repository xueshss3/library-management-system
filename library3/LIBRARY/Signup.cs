﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;
using BLL;

namespace LIBRARY
{
    public partial class Signup : Form
    {
        public Signup()
        {
            InitializeComponent();
        }

        private void signup_Load(object sender, EventArgs e)
        {

        }



        //登录按钮
        private void button1_Click(object sender, EventArgs e)
        {
           
            if(textBox2.Text!=textBox3.Text)
            {
                MessageBox.Show("输入的密码不一致，请重新输入");
            }
            else if(textBox2.Text== ""||textBox1.Text==""||textBox3.Text=="")
            {
                MessageBox.Show("信息不能为空,请重新输入","提示",MessageBoxButtons.OKCancel,MessageBoxIcon.Warning);
            }
            else
            {
                int res = 0;
                TransferAdmin user = new TransferAdmin();
                user.Name=textBox1.Text;
                user.Pwd=textBox2.Text;
                user.Role=comboBox1.SelectedItem.ToString();
                CheckAdminInformation  c1 = new CheckAdminInformation(user);
                res = c1.sign_up();
                if (res > 0)
                {
                    MessageBox.Show("注册成功");
                    Signin signin = new Signin();
                    this.Close();
                    signin.Show();

                }
                else
                {
                    MessageBox.Show("注册失败");
                }

            }
    
            
        }


        //注册按钮
        private void button2_Click(object sender, EventArgs e)
        {
            Signin signin = new Signin();
            signin.Show();
            this.Close();
        }
    }
}

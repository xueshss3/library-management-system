﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace DAL
{
    public class GetInformationfromSqlserver
    {
        private string connString;   //连接字符串
        private string cmdString;    //命令字符串
        private  SqlConnection conn = null;   //连接对象
        private  SqlCommand cmd = null;       //命令对象
        public GetInformationfromSqlserver()   //给连接字符串赋值
        {
            connString = ConfigurationManager.ConnectionStrings["libraryconn"].ConnectionString;  
        }
        public string CmdString        //给命令字符串赋值
        {
            set
            {
                cmdString = value;  
            }
        }
        public DataTable selectInfo()   //将查询结果赋给表，并返回表
        {
            conn = new SqlConnection(connString);
            cmd = new SqlCommand(cmdString, conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;

        }

        public int executeInfo()     //增，删，改，返回标志变量
        {
            int checkFlag;
            conn = new SqlConnection(connString);
            cmd = new SqlCommand(cmdString, conn);
            conn.Open();
            checkFlag = cmd.ExecuteNonQuery();
            conn.Close();
            return checkFlag;
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class TransferAdmin
    {
        private string name = null;
        private string pwd = null;
        private string role = null;
        public string Pwd { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
    }
}

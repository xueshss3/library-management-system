﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class TransferBook
    {
        private int bid;
        private string bname = null;
        private string bpublish = null;
        private string bauthor = null;
        private int bnumber;

        public int Bid{get;set;}
        public string Bname { get; set; }
        public string Bpublish { get; set; }
        public string Bauthor{ get; set; }
        public int  Bnumber { get; set; }

    }
}
